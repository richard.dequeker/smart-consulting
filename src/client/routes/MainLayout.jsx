import React, { lazy } from "react";
import { Switch, Route } from "react-router-dom";

import LazyPage from "./LazyPage";

const Home = lazy(() => import("../pages/Home"));
const HomePage = () => (
  <LazyPage>
    <Home />
  </LazyPage>
);
const Err404 = lazy(() => import("../pages/Err404"));
const Err404Page = () => (
  <LazyPage>
    <Err404 />
  </LazyPage>
);

const MainLayout = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route component={Err404Page} />
  </Switch>
);

export default MainLayout;
