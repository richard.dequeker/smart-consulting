import React, { Suspense } from 'react';
import PropTypes from 'prop-types';

import Spinner from '@components/atoms/Spinner';

const LazyPage = ({ children }) => (
    <Suspense fallback={<Spinner />}>
        {children}
    </Suspense>
);

LazyPage.propTypes = {
    children: PropTypes.node.isRequired
};

export default LazyPage;