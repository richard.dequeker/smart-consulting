import React from "react";

import PageTemplate from "@components/templates/PageTemplate";
import Header from "@components/organisms/Header";

import Three from "@components/Three";

const Err404 = () => (
  <PageTemplate header={<Header />}>
    <div style={{ width: "100%", height: "500px" }}>
      <Three />
    </div>
  </PageTemplate>
);

export default Err404;
