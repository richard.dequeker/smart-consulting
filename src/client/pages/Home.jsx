import React from "react";

import PageTemplate from "@components/templates/PageTemplate";
import Header from "@components/organisms/Header";
import Banner from "@components/organisms/Banner";
import Footer from "@components/organisms/Footer";

import ProjectSection from "@components/organisms/ProjectSection";
import TeamSection from "@components/organisms/TeamSection";

const Home = () => (
  <PageTemplate
    header={<Header />}
    footer={<Footer />}
    banner={<Banner url="/assets/images/triangle_gray.png" />}
  >
    <ProjectSection />
    <TeamSection />
  </PageTemplate>
);

export default Home;
