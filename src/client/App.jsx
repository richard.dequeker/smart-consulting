import React from "react";
import { BrowserRouter } from "react-router-dom";
import { hot, setConfig } from "react-hot-loader";
import { injectGlobal } from "react-emotion";

import MainLayout from "./routes/MainLayout";

setConfig({
  ignoreSFC: true,
  pureRender: true
});

injectGlobal`
html body {
    margin: 0 auto;
    background-color: white;
    * {
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }
}
`;

const App = () => (
  <BrowserRouter>
    <MainLayout />
  </BrowserRouter>
);

export default hot(module)(App);
