import styled, { keyframes } from "react-emotion";

const spin = keyframes`
form {
    transform: rotate(0);
}
to {
    transform: rotate(360deg);
}
`;

const Spinner = styled.div`
  position: absolute;
  top: calc(50vh - (35px / 2));
  left: calc(50vw - (35px / 2));
  margin: auto;
  width: 35px;
  height: 35px;
  border: solid 2px #5f4b8b;
  border-bottom-color: #262626;
  border-radius: 50%;
  animation: ${spin} 5s infinite linear;
`;

export default Spinner;
