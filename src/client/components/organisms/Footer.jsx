import React from "react";
import styled from "react-emotion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebookSquare,
  faInstagram,
  faLinkedin
} from "@fortawesome/free-brands-svg-icons";
import { Link } from "react-router-dom";

const Wrapper = styled.div`
  display: flex;
  background-color: #f2f2f2f2;
  color: white;
`;

const Logo = styled.div`
  background: url(${props => props.url});
  background-position: center;
  background-repeat: no-repeat;
  background-size: 90%;
  height: 40px;
  width: 40px;
  cursor: pointer;
`;

const Separator = styled.div`
  border: solid #008796 1px;
  height: 40px;
  margin: 1em;
`;

const Navigation = styled.div`
  display: flex;
  flex: 4;
  align-items: center;
  justify-content: space-between;
  margin: 1em;
  a {
    text-decoration: none;
    color: #008796;
    .contact {
      width: 210px;
      display: flex;
      justify-content: space-between;
      transition: color 250ms ease;
      cursor: pointer;
      &:hover {
        color: white;
      }
    }
  }
`;

const Social = styled.div`
  display: flex;
  margin-top: auto;
  margin-bottom: 1em;
  width: 90px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 1em;
  a {
    text-decoration: none;
    color: #008796;
    transition: color 250ms ease;
    cursor: pointer;
    &:hover {
      color: white;
    }
  }
`;

const Footer = () => (
  <Wrapper>
    <Navigation>
      <Logo url="/assets/images/logo_small.png" />
      <Link to="/">
        <div className="contact">
          <FontAwesomeIcon icon={faEnvelope} />
          hello@smartconsulting.fr
        </div>
      </Link>
    </Navigation>
    <Separator />
    <Social>
      <a href="https://www.facebook.com/">
        <FontAwesomeIcon icon={faFacebookSquare} />
      </a>
      <a href="https://www.instagram.com/">
        <FontAwesomeIcon icon={faInstagram} />
      </a>
      <a href="https://www.linkedin.com/">
        <FontAwesomeIcon icon={faLinkedin} />
      </a>
    </Social>
  </Wrapper>
);

export default Footer;
