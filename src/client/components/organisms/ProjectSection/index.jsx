import React from "react";
import styled from "react-emotion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  padding: 0 3em;
  min-height: 100vh;
  h2 {
    font-size: 28px;
    color: #008796;
  }
  h3 {
    font-size: 22px;
    color: #414141;
  }
`;

const ProjectGrid = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-left: auto;
  margin-right: auto;
`;

const ProjectItem = styled.div`
  position: relative;
  height: 400px;
  perspective: 1000px;
  &:hover > div {
    transform: rotateX(5deg);
  }
  &:hover .project-info {
    height: 170px;
    margin-top: 60px;
  }
`;

const ProjectCard = styled.div`
  height: 400px;
  overflow: hidden;
  border-radius: 4px;
  transition: transform 0.6s;
  transform-style: preserve-3d;
  width: 100%;
  background: url(${props => props.url});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  padding: 1em;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
`;

const ProjectLogo = styled.img`
  margin-left: 1em;
  margin-top: 1em;
  background-size: cover;
  background-position: center;
  width: 50%;
  height: 90px;
`;

const ProjectInnerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 0px;
  overflow: hidden;
  width: 100%;
  margin-top: 1000px;
  transition: height 250ms ease, margin-top 250ms ease;
`;

const ProjectInfo = styled.div`
  position: relative;
  margin-left: 1em;
  h4 {
    font-weight: bold;
    color: white;
  }
  p {
    color: white;
  }
`;

const ProjectLink = styled.div`
  position: relative;
  color: white;
  margin-top: auto;

  cursor: pointer;
  .arrow {
    margin-left: 0.5em;
    color: #008796;
    transition: color 250ms ease;
  }
  &:hover {
    .arrow {
      color: #971005;
    }
  }
`;

const InfoBackground = styled.div`
  z-index: -1;
  position: absolute;
  top: 100px;
  left: 0;
  background: #008796;
  width: 300px;
  height: 300px;
  transform-origin: center;
  transform: rotate(45deg) translateX(-150px);
`;

const Section = () => (
  <Wrapper>
    <h2>Nos projets</h2>
    <h3>Ce que nous avons réalisé</h3>
    <ProjectGrid>
      <ProjectItem>
        <ProjectCard url="/assets/images/lisbon-winner-astralis.jpg">
          <ProjectLogo src="/assets/images/blast_pro_logo_madrid.png" />
          <ProjectInnerWrapper className="project-info">
            <ProjectInfo>
              <InfoBackground />
              <div className="type">
                <h4>Type</h4>
                <p>Game, UX/UI</p>
              </div>
              <div className="client">
                <h4>Client</h4>
                <p>Blast pro series</p>
              </div>
            </ProjectInfo>
            <ProjectLink>
              Voir le projet
              <span className="arrow">
                <FontAwesomeIcon icon={faArrowRight} />
              </span>
            </ProjectLink>
          </ProjectInnerWrapper>
        </ProjectCard>
      </ProjectItem>
    </ProjectGrid>
  </Wrapper>
);

export default Section;
