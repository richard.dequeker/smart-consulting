import React, { Component } from "react";
import styled from "react-emotion";
import Navigation from "@components/organisms/Navigation";

const Wrapper = styled.div`
  top: ${props => (props.hide ? "-1000px" : 0)};
  overflow: hidden;
  transition: height 250ms ease-in-out;
  position: relative;
  display: flex;
  justify-content: space-between;
  padding: 0 2em;
  transition: top ease 250ms;
`;

const Logo = styled.div`
  margin-left: auto;
  background: url(${props => props.url});
  background-position: center;
  background-repeat: no-repeat;
  background-size: 90%;
  height: 100px;
  width: 100px;
  cursor: pointer;
`;

export default class Header extends Component {
  state = {
    scrollTop: 0,
    hide: false
  };

  componentDidMount() {
    window.addEventListener("scroll", this._handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this._handleScroll);
  }

  _handleScroll = () => {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    if (scrollTop > this.state.scrollTop) {
      this._handleScrollDown();
    } else {
      this._handleScrollUp();
    }
    this.setState({ scrollTop: scrollTop <= 0 ? 0 : scrollTop });
  };

  _handleScrollDown = () => {
    this.setState({ hide: true });
  };

  _handleScrollUp = () => {
    this.setState({ hide: false });
  };

  render() {
    return (
      <>
        <Navigation hide={this.state.hide} />
        <Wrapper hide={this.state.hide}>
          <Logo url="/assets/images/logo_big.png" />
        </Wrapper>
      </>
    );
  }
}
