import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
import { slide as Menu } from "react-burger-menu";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faGamepad } from "@fortawesome/free-solid-svg-icons";

import "./styles.scss";

const Navigation = ({ pageWrapId, isOpen, right, hide }) => (
  <Menu
    pageWrapId={pageWrapId}
    isOpen={isOpen}
    right={right}
    burgerButtonClassName={hide ? "bm-burger-button--hide" : "bm-burger-button"}
  >
    <NavLink id="home" activeClassName="menu-item--active" to="/">
      <span>
        <FontAwesomeIcon icon={faHome} />
      </span>
      <span>Home</span>
    </NavLink>
    <NavLink id="blast" activeClassName="menu-item--active" to="/blast">
      <span>
        <FontAwesomeIcon icon={faGamepad} />
      </span>
      <span>Blast</span>
    </NavLink>
  </Menu>
);

Navigation.propTypes = {
  pageWrapId: PropTypes.string,
  isOpen: PropTypes.bool,
  right: PropTypes.bool,
  hide: PropTypes.bool
};

export default Navigation;
