import React from "react";
import styled from "react-emotion";

const Wrapper = styled.div`
  position: relative;
  width: 100%;
  padding: 0 3em;
  min-height: 100vh;
  margin-bottom: 1em;
  h2 {
    font-size: 28px;
    color: #008796;
  }
`;

const TeamGrid = styled.div`
  width: 100%;
  max-width: 
  margin-left: auto;
  margin-right: auto;
  min-height: 400px;
  width: 100%;
  display: grid;
  grid-gap: 1em;
  grid-template-columns: repeat(9, minmax(10px, 350px));
  grid-template-rows: repeat(10, 100px);
  grid-template-areas:
    "a a a . . . c c c"
    "a a a . . . c c c"
    "a a a b b b c c c"
    "a a a b b b c c c"
    "d d d b b b f f f"
    "d d d b b b f f f"
    "d d d e e e f f f"
    "d d d e e e f f f"
    ". . . e e e . . ."
    ". . . e e e . . .";

  .one {
    grid-area: a;
  }
  .two {
    grid-area: b;
  }
  .three {
    grid-area: c;
  }
  .four {
    grid-area: d;
  }
  .five {
    grid-area: e;
  }
  .six {
    grid-area: f;
  }

  @media screen and (max-width: 900px) {
    display: flex;
    flex-direction: column;
  }
`;

const TeamMember = styled.div`
  cursor: pointer;
  position: relative;
  border-radius: 4px;
  overflow: hidden;
  &:hover {
    .member-info {
      top: 0;
    }
  }
  @media screen and (max-width: 900px) {
    display: flex;
    flex-direction: column;
    width: 350px;
    height: 400px;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 1em;
    .member-info {
      top: 320px;
    }
  }
`;

const PhotoBackground = styled.div`
  filter: grayscale(100%);
  z-index: -1;
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  background: url(${props => props.url});
  background-size: cover;
  background-repeat: no-repeat;
  border-radius: 4px;
`;

const MemberInfo = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  top: 375px;
  z-index: 1;
  padding-left: 1em;
  transition: top 250ms ease-in-out;
  h2 {
    color: #008796;
    margin-left: 1em;
  }
  h3 {
    font-size: 22px;
    color: white;
  }
  p {
    margin-top: 70%;
    margin-bottom: auto;
    color: white;
  }
`;

const TeamSection = () => (
  <Wrapper>
    <h2>Notre équipe</h2>
    <h3>Ce que nous sommes</h3>
    <TeamGrid>
      <TeamMember className="one">
        <PhotoBackground url="/assets/team/marion.png" />
        <MemberInfo className="member-info">
          <h2>Marion S.</h2>
          <h3>- Designer graphique</h3>
          <p>
            Toujours dans le but de créer des expériences numériques
            engageantes, en gardant toujours à l'esprit l'utilisateur.
          </p>
        </MemberInfo>
      </TeamMember>
      <TeamMember className="two">
        <PhotoBackground url="/assets/team/nathan.png" />
        <MemberInfo className="member-info">
          <h2>Nathan F.</h2>
          <h3>- Administrateur Réseau</h3>
          <p>
            La sécurité avant tout ! Que ce soit pour l’utilisateur ou pour
            l’entreprise.
          </p>
        </MemberInfo>
      </TeamMember>
      <TeamMember className="three">
        <PhotoBackground url="/assets/team/maxime.png" />
        <MemberInfo className="member-info">
          <h2>Maxime F.</h2>
          <h3>- Expert réseau</h3>
          <p>Prêt à apporter des solutions efficaces.</p>
        </MemberInfo>
      </TeamMember>
      <TeamMember className="four">
        <PhotoBackground url="/assets/team/richard.jpg" />
        <MemberInfo className="member-info">
          <h2>Richard D.</h2>
          <h3>- Développeur logiciel et web</h3>
          <p>
            Plusieurs années d’expérience dans le management d’équipe.
            Développeur confirmé sur plusieurs techno web.
          </p>
        </MemberInfo>
      </TeamMember>
      <TeamMember className="five">
        <PhotoBackground url="/assets/team/baptiste.png" />
        <MemberInfo className="member-info">
          <h2>Baptiste I.</h2>
          <h3>
            - Ingénieur architecture informatique et système d’information
          </h3>
          <p>Expert en conception de site web, logiciel, etc ...</p>
        </MemberInfo>
      </TeamMember>
      <TeamMember className="six">
        <PhotoBackground url="/assets/team/mathieu.png" />
        <MemberInfo className="member-info">
          <h2>Mathieu D.</h2>
          <h3>- Développeur web</h3>
          <p>Jeune développeur web, passionné et investie.</p>
        </MemberInfo>
      </TeamMember>
    </TeamGrid>
  </Wrapper>
);

export default TeamSection;
