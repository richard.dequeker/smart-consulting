import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookSquare,
  faInstagram,
  faLinkedin
} from "@fortawesome/free-brands-svg-icons";

import { Wrapper, Social } from "./styles";

const Banner = ({ url }) => (
  <Wrapper url={url}>
    <p>
      <span className="title">Smart consulting </span>
      est un studio numérique où le design stratégique et la technologie
      s&#39;unissent dans les produits de demain.
    </p>
    <Social>
      <a href="https://www.facebook.com/">
        <FontAwesomeIcon icon={faFacebookSquare} />
      </a>
      <a href="https://www.instagram.com/">
        <FontAwesomeIcon icon={faInstagram} />
      </a>
      <a href="https://www.linkedin.com/">
        <FontAwesomeIcon icon={faLinkedin} />
      </a>
    </Social>
  </Wrapper>
);

Banner.propTypes = {
  url: PropTypes.string.isRequired
};

export default Banner;
