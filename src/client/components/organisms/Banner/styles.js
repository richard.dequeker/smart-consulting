import styled from "react-emotion";

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background: url(${props => props.url});
  background-size: cover;
  padding: 2em;
  padding-top: 40vh;
  font-weight: bold;
  font-size: 32px;
  display: flex;
  flex-direction: column;
  p {
    position: relative;
    width: 70%;
    .title {
      color: #008796;
    }
    color: white;
  }
`;

export const Social = styled.div`
  display: flex;
  margin-top: auto;
  margin-bottom: 1em;
  color: white;
  width: 150px;
  display: flex;
  justify-content: space-between;
  a {
    text-decoration: none;
    color: white;
    transition: color 250ms ease;
    &:hover {
      color: #008796;
    }
  }
`;
