import React from "react";
import PropTypes from "prop-types";
import styled from "react-emotion";

const Wrapper = styled.div`
  min-height: calc(100vh - 2px);
  display: flex;
  flex-direction: column;
  position: relative;
  width: 100%;
  main {
    flex: 1;
    display: flex;
  }
`;

const Header = styled.div`
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 999;
`;

const Banner = styled.div`
  height: 100vh;
  width: 100%;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const PageTemplate = ({ header, banner, footer, children }) => (
  <Wrapper id="page-wrap">
    <Header>{header}</Header>
    {banner && <Banner>{banner}</Banner>}
    <Content>{children}</Content>
    <footer>{footer}</footer>
  </Wrapper>
);

PageTemplate.propTypes = {
  header: PropTypes.node.isRequired,
  banner: PropTypes.node,
  footer: PropTypes.node,
  children: PropTypes.node
};

export default PageTemplate;
