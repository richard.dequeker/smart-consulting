import React, { Component, createRef } from "react";
import styled from "react-emotion";
import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  MeshPhongMaterial,
  Mesh,
  BoxGeometry,
  FontLoader,
  TextGeometry,
  HemisphereLight,
  Fog,
  Color,
  TextureLoader,
  RepeatWrapping,
  ShaderMaterial,
  SphereBufferGeometry,
  BufferAttribute,
  Math as ThreeMath
} from "three";

const fontLoader = new FontLoader();

const Wrapper = styled.div`
  flex: 1;
  width: 100%;
  height: calc(100vh - 3.5px);
  canvas {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
  }
`;

const vertexShader = `
			uniform float amplitude;
			attribute float displacement;
			varying vec3 vNormal;
			varying vec2 vUv;
			void main() {
				vNormal = normal;
				vUv = ( 0.5 + amplitude ) * uv + vec2( amplitude );
				vec3 newPosition = position + amplitude * normal * vec3( displacement );
				gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );
			}
`;

const fragmentShader = `
 varying vec3 vNormal;
			varying vec2 vUv;
			uniform vec3 color;
			uniform sampler2D texture;
			void main() {
				vec3 light = vec3( 0.5, 0.2, 1.0 );
				light = normalize( light );
				float dProd = dot( vNormal, light ) * 0.5 + 0.5;
				vec4 tcolor = texture2D( texture, vUv );
				vec4 gray = vec4( vec3( tcolor.r * 0.3 + tcolor.g * 0.59 + tcolor.b * 0.11 ), 1.0 );
				gl_FragColor = gray * vec4( vec3( dProd ) * vec3( color ), 1.0 );
      }
`;
export default class ReactThree extends Component {
  constructor() {
    super();
    this.container = createRef();
    this.scene;
    this.camera;
    this.fog;
    this.renderer;
    this.geometry;
    this.material;
    this.box;
    this.textGeometry;
    this.textMaterial;
    this.textMesh;
    this.pointLight;
    this.mouseX = 0;
    this.mouseY = 0;
  }

  componentDidMount() {
    const { width, height } = this.container.current.getBoundingClientRect();

    this.scene = new Scene();
    this.scene.fog = new Fog(0xffffff, 1, 1500);

    this.camera = new PerspectiveCamera(75, width / height, 0.1, 1000);
    this.camera.position.z = 100;

    this.renderer = new WebGLRenderer({ alpha: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(width, height);
    this.container.current.appendChild(this.renderer.domElement);

    this.geometry = new BoxGeometry(50, 50, 50);
    this.material = new MeshPhongMaterial({ color: 0x5f4b8b });
    this.cube = new Mesh(this.geometry, this.material);

    fontLoader.load("/assets/fonts/Ubuntu_Regular.json", font => {
      this.textGeometry = new TextGeometry("R.D", {
        font,
        height: 5,
        curveSegments: 12,
        bevelEnabled: true,
        bevelThickness: 5,
        bevelSize: 5,
        bevelSegments: 5
      });
      this.textMaterial = new MeshPhongMaterial({ color: 0x5f4b8b });
      this.textMesh = new Mesh(this.textGeometry, this.textMaterial);
      this.textMesh.position.z = -300;
      this.textMesh.position.x = -50;
      // this.scene.add(this.textMesh);
    });

    this.light = new HemisphereLight(0xffffff, 0x5f4b8b, 1);

    this.uniforms = {
      amplitude: { value: 1.0 },
      color: { value: new Color(0xff2200) },
      texture: { value: new TextureLoader().load("/assets/textures/water.jpg") }
    };

    this.uniforms.texture.value.wrapS = this.uniforms.texture.value.wrapT = RepeatWrapping;

    this.shaderMaterial = new ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: vertexShader,
      fragmentShader: fragmentShader
    });

    this.shaderGeometry = new SphereBufferGeometry(50, 128, 64);

    this.displacement = new Float32Array(
      this.shaderGeometry.attributes.position.count
    );
    this.noise = new Float32Array(
      this.shaderGeometry.attributes.position.count
    );
    for (var i = 0; i < this.displacement.length; i++) {
      this.noise[i] = Math.random() * 5;
    }
    this.shaderGeometry.addAttribute(
      "displacement",
      new BufferAttribute(this.displacement, 1)
    );

    this.sphere = new Mesh(this.shaderGeometry, this.shaderMaterial);

    // this.scene.add(this.cube);
    this.scene.add(this.sphere);
    this.scene.add(this.light);

    this._animate();
    window.onresize = this._handleWindowResize;
  }

  render() {
    return (
      <Wrapper onMouseMove={this._handleMouseMove} innerRef={this.container} />
    );
  }

  _animate = () => {
    requestAnimationFrame(this._animate);
    const time = Date.now() * 0.01;

    this.cube.rotation.x += 0.01;
    this.cube.rotation.y += 0.01;
    this.camera.position.x += (this.mouseX - this.camera.position.x) * 0.05;
    this.camera.position.y += (-this.mouseY - this.camera.position.y) * 0.05;
    this.camera.lookAt(this.scene.position);

    this.sphere.rotation.y = this.sphere.rotation.z = 0.01 * time;
    this.uniforms.amplitude.value =
      2.5 * Math.sin(this.sphere.rotation.y * 0.125);
    this.uniforms.color.value.offsetHSL(0.0005, 0, 0);

    for (var i = 0; i < this.displacement.length; i++) {
      this.displacement[i] = Math.sin(0.1 * i + time);
      this.noise[i] += 0.5 * (0.5 - Math.random());
      this.noise[i] = ThreeMath.clamp(this.noise[i], -5, 5);
      this.displacement[i] += this.noise[i];
    }
    this.sphere.geometry.attributes.displacement.needsUpdate = true;

    this.renderer.render(this.scene, this.camera);
  };

  _handleWindowResize = () => {
    const { width, height } = this.container.current.getBoundingClientRect();
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(width, height);
  };

  _handleMouseMove = e => {
    const { width, height } = this.container.current.getBoundingClientRect();
    const halfX = width / 2;
    const halfY = height / 2;
    this.mouseX = (e.clientX - halfX) / 2;
    this.mouseY = (e.clientY - halfY) / 2;
  };
}
