import React, { Component, createContext } from "react";

export const theme = {};

theme.palette = {
  primary: ["#008796"],
  danger: ["#971005"]
};

export const ThemeContext = createContext();

export class ThemeProvider extends Component {
  render() {
    return <ThemeContext.Provider />;
  }
}
