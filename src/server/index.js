/**
 * Express application
 * @description
 * Should connect to Mongodb
 * apply middlewares (security, routes ...)
 * listen on given port
 */
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import fallback from "express-history-api-fallback";

import path from "path";

const API_PORT = process.env.API_PORT || 3000;
const app = express();

const ROOT = path.resolve(process.cwd(), "dist/client");
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(ROOT));

app.use("*", fallback("index.html", { root: ROOT }));

app.listen(API_PORT, () =>
  console.log(`🚀  Server ready at http://localhost:${API_PORT}`)
);
