const { server, client } = require("./webpack.common");

// server dev config
server.mode = "production";

client.mode = "production";

module.exports = [server, client];
